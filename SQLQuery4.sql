create database binusmaya

use binusmaya


create table Roles(
	roleId int identity(1,1) primary key,
	name varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table Users(
	usersId int identity(1,1) primary key,
	roleId int foreign key references roles(roleId),
	name varchar(255) NOT NULL,
	email varchar(255) unique NOT NULL,
	password varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)


create table Message(
	messageId int identity(1,1) primary key,
	userId int foreign key references Users(UsersId),
	messagefrom varchar(255) NOT NULL,
	messagesubject varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table CoursesCategory(
	coursesCategoryId int identity(1,1) primary key,
	coursesCategoryName varchar(255) NOT NULL,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Courses(
	coursesHeaderId int identity(1,1) primary key,
	coursesCategoryId int foreign key references CoursesCategory(coursesCategoryId),
	userId int foreign key references Users(UsersId),
	coursesName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table LearningCategory(
	learningCategoryId int identity(1,1) primary key,
	learningCategoryName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table Learning(
	learningId int identity(1,1) primary key,
	learningCategoryId int foreign key references LearningCategory(learningCategoryId),
	userId int foreign key references Users(UsersId),
	learningName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table ForumCategory(
	forumCategoryId int identity(1,1) primary key,
	forumCategoryName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table ForumHeader(
	forumId int identity(1,1) primary key,
	forumCategoryId int foreign key references ForumCategory(forumCategoryId),
	forumName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table ForumDetail(
	forumDetailId int identity(1,1) primary key,
	forumId int foreign key references ForumHeader(forumId),
	userId int foreign key references Users(UsersId),
	content varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
) 


create table RegistrationCategory(
	registrationCategoryId int identity(1,1) primary key,
	registrationCategoryName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table Registration(
	registrationId int identity(1,1) primary key,
	registrationCategoryId int foreign key references RegistrationCategory(registrationCategoryId),
	userId int foreign key references Users(UsersId),
	registrationName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table FeedbackHeader(
	feedbackId int identity(1,1) primary key,
	feedbackName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table FeedbackDetail(
	feedbackDetailId int identity(1,1) primary key,
	feedbackId int foreign key references FeedbackHeader(feedbackId),
	userId int foreign key references Users(UsersId),
	content varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table Financial(
	financialId int identity(1,1) primary key,
	userId int foreign key references Users(UsersId),
	financialName varchar(255) NOT NULL,
	financialStatus varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table SupportCategory(
	supportCategoryId int identity(1,1) primary key,
	supportCategoryName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)

create table Support(
	supportId int identity(1,1) primary key,
	supportCategoryId int foreign key references SupportCategory(supportCategoryId),
	userId int foreign key references Users(UsersId),
	supportName varchar(255) NOT NULL,
	createdAt datetimeoffset default getdate(),
	createdBy varchar(255) default 'SYSTEM',	
	updatedAt datetimeoffset default getdate(),
	updatedBy varchar(255) default 'SYSTEM'
)


